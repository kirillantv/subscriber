# Тестовое задание "Жизненный цикл абонента"

### Get started

В качестве базы данных для простоты была применена H2Database. Инструмент миграции - flyway.

Для того, чтобы применить миграцию схем базы данных и фикстур абонентов и тарифов, лежащих в каталоге 
*resources/db/migration*:

``mvn flyway:migrate -Dflyway.url=jdbc:h2:file:~/data/demo -Dflyway.user=sa -Dflyway.password=password``

scДля конфигурации лимита звонков при запуске приложения нужно передавать параметр *subscriber.callLimit*:

``--subscriber.callLimit=7``

По умолчанию, он равен 5.

------------------

### API Reference

#### Метод получения всех абонентов
`GET /rest/subscriber` 

----------------

#### Метод получения баланса

`GET /rest/subscriber/info?msisdn=:msisdn`

| Свойство | Описание |
|:--------------------|:--------------------------:|
| msisdn | Номер абонента |

##### Пример ответа

``` json
{
     "id": 1, // ID абонента
     "msisdn": "79996663322", // Номер абонента
     "name": "Леонид", // Имя абонента
     "surname": "Петров", // Фамилия абонента
     "balance": 29300, // Баланс абонента в копейках
     "createdAt": "2019-10-27T14:40:38.983", // Дата создания абонента
     "updatedAt": "2019-10-27T16:44:55.117", // Дата обновления информации об абоненте
     "status": {
         "id": 1, // ID статуса абонента
         "name": "ACTIVE" // Статус абонента
     },
     "tariff": {
         "id": 1, // ID тарифа абонента
         "name": "Тест тариф v1.0", // Название тарифа абонента
         "params": {
             "callPrice": 150, // Стоимость звонка в копейках
             "smsPrice": 100 // Стоимость СМС в копейках
         }
     }
}
```
##### Ошибки

|  Сообщение           | HTTP Status Code| Описание   |
|:--------------------|:--------------------------|:------------:|
|  Абонент не найден |    404   | Абонента с переданным msisdn нет в базе |

-----------------------------------------
#### Метод звонка

`POST /rest/subscriber/call`

##### Тело запроса

Пример запроса:
```json
{
	"msisdn": "79952331221", // Номер звонящего абонента
	"targetMsisdn": "79998885533" // Кому звонит абонент
}
```

##### Ответ:
200 OK

##### Ошибки

|  Сообщение           | HTTP Status Code|
|:--------------------|:--------------------------:|
|  Абонент не найден |    404   |
|  Операция не поддерживается тарифом |    403   |
|  Абонент заблокирован |    403   |
|  Лимит звонков для абонента :msisdn исчерпан |    403   |
|  Некорректное тело запроса |    400   |

----------------
#### Метод отправки СМС

`POST /rest/subscriber/sms`

##### Тело запроса

Пример запроса:
```json
{
	"msisdn": "79952331221", // Номер абонента
	"targetMsisdn": "79998885533", // Адресат сообщения
	"message": "Hello, world!" // Текст сообщения
}
```
##### Ответ:
200 OK

##### Ошибки

|  Сообщение           | HTTP Status Code|
|:--------------------|:--------------------------:|
|  Абонент не найден |    404   |
|  Операция не поддерживается тарифом |    403   |
|  Абонент заблокирован |    403   |
|  Некорректное тело запроса |    400   |

-------------
#### Метод пополнения баланса

`POST /rest/subscriber/replenish-balance`

##### Тело запроса

Пример запроса:
```json
{
	"msisdn": "79952331221", // Номер абонента
	"amount": 1000 // Сумма пополнения в копейках
}
```

##### Ответ:
200 OK

##### Ошибки

|  Сообщение           | HTTP Status Code|
|:--------------------|:--------------------------:|
|  Некорректное тело запроса |    400   |


