package com.nexigntest.subscriber.service;

import com.nexigntest.subscriber.entity.Subscriber;
import com.nexigntest.subscriber.entity.SubscriberStatus;
import com.nexigntest.subscriber.entity.Tariff;
import com.nexigntest.subscriber.exception.SubscriberIsBlockedException;
import com.nexigntest.subscriber.exception.SubscriberNotFoundException;
import com.nexigntest.subscriber.model.ReplenishBalanceRequest;
import com.nexigntest.subscriber.model.SubscriberCallRequest;
import com.nexigntest.subscriber.model.SubscriberSmsRequest;
import com.nexigntest.subscriber.repository.SubscriberActionHistoryRepository;
import com.nexigntest.subscriber.repository.SubscriberRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class SubscriberServiceTest {

    @Autowired
    private SubscriberService subscriberService;

    @MockBean
    SubscriberRepository subscriberRepository;

    @MockBean
    SubscriberActionHistoryRepository subscriberActionHistoryRepository;

    Subscriber activeSubscriber;
    Subscriber blockedSubscriber;

    @BeforeEach
    public void setUp() {
        SubscriberStatus activeStatus = new SubscriberStatus();
        activeStatus.setId(1);
        activeStatus.setName("ACTIVE");

        SubscriberStatus blockedStatus = new SubscriberStatus();
        blockedStatus.setId(2);
        blockedStatus.setName("BLOCKED");

        HashMap<String, Object> tariffParams = new HashMap<>();
        tariffParams.put("callPrice", 100);
        tariffParams.put("smsPrice", 50);

        Tariff tariff = new Tariff();
        tariff.setId(1);
        tariff.setName("Тест тариф 1");
        tariff.setParams(tariffParams);

        Subscriber activeSubscriber = new Subscriber();

        activeSubscriber.setId(1L);
        activeSubscriber.setBalance(1000);
        activeSubscriber.setStatus(activeStatus);
        activeSubscriber.setCreatedAt(LocalDateTime.now());
        activeSubscriber.setName("Василий");
        activeSubscriber.setSurname("Алибабаевич");
        activeSubscriber.setMsisdn("79863332211");
        activeSubscriber.setTariff(tariff);

        Subscriber blockedSubscriber = new Subscriber();
        blockedSubscriber.setId(1L);
        blockedSubscriber.setBalance(-1000);
        blockedSubscriber.setStatus(blockedStatus);
        blockedSubscriber.setCreatedAt(LocalDateTime.now());
        blockedSubscriber.setName("John");
        blockedSubscriber.setSurname("Doe");
        blockedSubscriber.setMsisdn("79863334556");
        blockedSubscriber.setTariff(tariff);

        this.blockedSubscriber = blockedSubscriber;
        this.activeSubscriber = activeSubscriber;
    }

    @Test
    public void whenValidMsisdn_thenSubscriberIsFoundTest() {
        Mockito.when(subscriberRepository.findSubscriberByMsisdn(activeSubscriber.getMsisdn()))
                .thenReturn(activeSubscriber);

        String msisdn = activeSubscriber.getMsisdn();
        Subscriber subscriber = subscriberService.getSubscriberByMsisdn(msisdn);

        assertThat(subscriber.getMsisdn(), equalTo(msisdn));
    }

    @Test
    public void whenNonexistantMsisdn_thenSubscriberNotFoundException() {
        Mockito.when(subscriberRepository.findSubscriberByMsisdn(activeSubscriber.getMsisdn()))
                .thenReturn(activeSubscriber);

        String msisdn = "79886663322";
        assertThrows(SubscriberNotFoundException.class, () -> subscriberService.getSubscriberByMsisdn(msisdn));
    }

    @Test
    public void whenGetAllSubscribers_thenReturns2Subscribers() {
        Mockito.when(subscriberRepository.findAll()).thenReturn(Arrays.asList(activeSubscriber, blockedSubscriber));

        List<Subscriber> subscriberList = subscriberService.getSubscribers();

        assertThat(subscriberList.size(), equalTo(2));
    }

    @Test
    public void whenBlockedSubscriberCalls_thenReturnsSubscriberIsBlockedException() {
        Mockito.when(subscriberRepository.findSubscriberByMsisdn(blockedSubscriber.getMsisdn()))
                .thenReturn(blockedSubscriber);

        String blockedMsisdn = blockedSubscriber.getMsisdn();
        SubscriberCallRequest subscriberCallRequest = new SubscriberCallRequest();
        subscriberCallRequest.setMsisdn(blockedMsisdn);
        subscriberCallRequest.setTargetMsisdn("78996633201");

        assertThrows(SubscriberIsBlockedException.class, () -> subscriberService.makeCall(subscriberCallRequest));
    }

    @Test
    public void whenBlockedSubscriberSendSms_thenReturnsSubscriberIsBlockedException() {
        Mockito.when(subscriberRepository.findSubscriberByMsisdn(blockedSubscriber.getMsisdn()))
                .thenReturn(blockedSubscriber);

        SubscriberSmsRequest subscriberSmsRequest = new SubscriberSmsRequest();
        subscriberSmsRequest.setMsisdn(blockedSubscriber.getMsisdn());
        subscriberSmsRequest.setTargetMsisdn("79963210022");
        subscriberSmsRequest.setMessage("Test message");

        assertThrows(SubscriberIsBlockedException.class, () -> subscriberService.sendSms(subscriberSmsRequest));
    }

    @Test
    public void whenSubscriberReplenishBalance_thenBalanceIsChanging() {
        Mockito.when(subscriberRepository.findSubscriberByMsisdn(blockedSubscriber.getMsisdn()))
                .thenReturn(blockedSubscriber);

        Integer amount = 3000;
        Integer initialBalance = blockedSubscriber.getBalance();

        ReplenishBalanceRequest replenishBalanceRequest = new ReplenishBalanceRequest();
        replenishBalanceRequest.setMsisdn(blockedSubscriber.getMsisdn());
        replenishBalanceRequest.setAmount(amount);

        subscriberService.replenishBalance(replenishBalanceRequest);

        Subscriber subscriber = subscriberService.getSubscriberByMsisdn(blockedSubscriber.getMsisdn());

        assertThat(subscriber.getBalance(), equalTo(initialBalance + amount));
    }
}
