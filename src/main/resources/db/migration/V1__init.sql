CREATE TABLE subscriber (
    id bigint auto_increment PRIMARY KEY,
    msisdn varchar(12) not null,
    name varchar(255) not null,
    surname varchar(255) not null,
    balance integer not null default 0,
    status_id integer not null default 1,
    tariff_id bigint not null,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp
);

CREATE TABLE subscriber_status (
    id int auto_increment primary key,
    name varchar(255) not null
);

INSERT INTO subscriber_status (name) VALUES ('ACTIVE'), ('BLOCKED');

ALTER TABLE subscriber
ADD FOREIGN KEY (status_id)
REFERENCES subscriber_status (id);

CREATE TABLE tariff (
    id int auto_increment primary key,
    name varchar(255) not null,
    params clob not null
);

ALTER TABLE subscriber
ADD FOREIGN KEY (tariff_id)
REFERENCES tariff (id);

CREATE TABLE subscriber_action_history (
    id identity primary key,
    subscriber_id bigint not null,
    action_type enum('CALL', 'SMS') not null,
    processed_at timestamp not null
);

ALTER TABLE subscriber_action_history
ADD FOREIGN KEY (subscriber_id)
REFERENCES subscriber (id);

CREATE INDEX idx_subscriber_action_history_processed_at ON subscriber_action_history(processed_at);