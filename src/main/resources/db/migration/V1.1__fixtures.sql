INSERT INTO tariff (name, params)
VALUES
       ('Тест тариф v1.0', '{"callPrice":150, "smsPrice":100}'), ('Тест тариф v2.0', '{"callPrice":300, "smsPrice":50}');

INSERT INTO subscriber (msisdn, name, surname, balance, status_id, tariff_id)
VALUES
       ('79996663322', 'Леонид', 'Петров', 30000, 1, 1),
       ('79952331221', 'Василий', 'Николаев', 500, 1, 2),
       ('79956314455', 'Авраам', 'Абрамов', 10000, 1, 1);