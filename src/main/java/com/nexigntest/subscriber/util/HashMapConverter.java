package com.nexigntest.subscriber.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class HashMapConverter implements AttributeConverter<Map<String, Object>, String> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(Map<String, Object> tariffParams) {

        String tariffParamsJson = null;
        try {
            tariffParamsJson = objectMapper.writeValueAsString(tariffParams);
        } catch (final JsonProcessingException e) {
            log.error("JSON writing error", e);
        }

        return tariffParamsJson;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String tariffParamsJson) {

        Map<String, Object> tariffParams= null;
        try {
            tariffParams = objectMapper.readValue(tariffParamsJson, Map.class);
        } catch (final IOException e) {
            log.error("JSON reading error", e);
        }

        return tariffParams;
    }

}
