package com.nexigntest.subscriber.service;

import com.nexigntest.subscriber.entity.ActionType;
import com.nexigntest.subscriber.entity.Subscriber;
import com.nexigntest.subscriber.entity.SubscriberActionHistory;
import com.nexigntest.subscriber.entity.SubscriberStatus.StatusEnum;
import com.nexigntest.subscriber.entity.Tariff;
import com.nexigntest.subscriber.exception.ForbiddenOperationException;
import com.nexigntest.subscriber.exception.SubscriberIsBlockedException;
import com.nexigntest.subscriber.exception.SubscriberNotFoundException;
import com.nexigntest.subscriber.model.ReplenishBalanceRequest;
import com.nexigntest.subscriber.model.SubscriberCallRequest;
import com.nexigntest.subscriber.model.SubscriberSmsRequest;
import com.nexigntest.subscriber.repository.SubscriberActionHistoryRepository;
import com.nexigntest.subscriber.repository.SubscriberRepository;
import com.nexigntest.subscriber.repository.SubscriberStatusRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.nexigntest.subscriber.entity.SubscriberStatus.StatusEnum.BLOCKED;

@Slf4j
@Service
public class SubscriberService {

    @Value("${subscriber.callLimit}")
    private Integer callLimit;

    private final SubscriberRepository subscriberRepository;

    private final SubscriberActionHistoryRepository subscriberActionHistoryRepository;

    private final SubscriberStatusRepository subscriberStatusRepository;

    public SubscriberService(SubscriberRepository subscriberRepository, SubscriberActionHistoryRepository subscriberActionHistoryRepository, SubscriberStatusRepository subscriberStatusRepository) {
        this.subscriberRepository = subscriberRepository;
        this.subscriberActionHistoryRepository = subscriberActionHistoryRepository;
        this.subscriberStatusRepository = subscriberStatusRepository;
    }

    public Subscriber getSubscriberByMsisdn(String msisdn) {
        log.info(String.format("Поиск абонента с msisdn = %s", msisdn));
        Subscriber subscriber = subscriberRepository.findSubscriberByMsisdn(msisdn);

        if (subscriber == null) {
            throw new SubscriberNotFoundException();
        }
        return subscriber;
    }

    public List<Subscriber> getSubscribers() {
        return subscriberRepository.findAll();
    }

    public void makeCall(SubscriberCallRequest subscriberCallRequest) {
        Subscriber subscriber = getSubscriberByMsisdn(subscriberCallRequest.getMsisdn());

        log.info(String.format("Абонент %s пытается позвонить по номеру %s",
                subscriber.getMsisdn(), subscriberCallRequest.getTargetMsisdn()));

        checkSubscriberCanCall(subscriber);

        log.info(String.format("Абонент звонит по номеру %s", subscriberCallRequest.getTargetMsisdn()));

        Integer servicePrice = (Integer) subscriber.getTariff().getParams().get("callPrice");
        processBillingDeduction(subscriber, servicePrice);

        saveActionInHistory(subscriber, ActionType.CALL);

        log.info(String.format("Текущий баланс абонента %s равен %s", subscriber.getMsisdn(), subscriber.getBalance()));
    }

    public void sendSms(SubscriberSmsRequest subscriberSmsRequest) {
        Subscriber subscriber = getSubscriberByMsisdn(subscriberSmsRequest.getMsisdn());

        log.info(String.format("Абонент %s пытается отправить СМС по номеру %s",
                subscriber.getMsisdn(), subscriberSmsRequest.getTargetMsisdn()));

        checkSubscriberCanSendSms(subscriber);
        log.info(String.format("Абонент %s отправил СМС по номеру %s", subscriber.getMsisdn(),
                subscriberSmsRequest.getTargetMsisdn()));

        Integer servicePrice = (Integer) subscriber.getTariff().getParams().get("smsPrice");
        processBillingDeduction(subscriber, servicePrice);

        saveActionInHistory(subscriber, ActionType.SMS);

        log.info(String.format("Текущий баланс абонента %s равен %s", subscriber.getMsisdn(), subscriber.getBalance()));
    }

    public void replenishBalance(ReplenishBalanceRequest replenishBalanceRequest) {
        Subscriber subscriber = getSubscriberByMsisdn(replenishBalanceRequest.getMsisdn());

        log.info(String.format("Абонент %s собирается пополнить свой баланс на %s руб.", subscriber.getMsisdn(),
                replenishBalanceRequest.getAmount() / 100));

        subscriber.setBalance(subscriber.getBalance() + replenishBalanceRequest.getAmount());

        if (isSubscriberBlocked(subscriber) && subscriber.getBalance() > 0) {
            subscriber.setStatus(subscriberStatusRepository.findSubscriberStatusByName(StatusEnum.ACTIVE.name()));
        }

        subscriberRepository.save(subscriber);

        log.info("Абонент успешно пополнил баланс");
    }

    private void saveActionInHistory(Subscriber subscriber, ActionType actionType) {
        SubscriberActionHistory subscriberActionHistory = new SubscriberActionHistory();
        subscriberActionHistory.setActionType(actionType);
        subscriberActionHistory.setProcessedAt(LocalDateTime.now());
        subscriberActionHistory.setSubscriber(subscriber);

        subscriberActionHistoryRepository.save(subscriberActionHistory);
    }

    private void processBillingDeduction(Subscriber subscriber, Integer servicePrice) {
        subscriber.setBalance(subscriber.getBalance() - servicePrice);

        if (subscriber.getBalance() < 0) {
            subscriber.setStatus(subscriberStatusRepository.findSubscriberStatusByName(BLOCKED.name()));
        }

        subscriberRepository.save(subscriber);
    }

    private void checkSubscriberCanCall(Subscriber subscriber) {
        if (!isOperationSupportedByTariff(subscriber.getTariff(), "callPrice")) {
            throw new ForbiddenOperationException();
        }

        if (isSubscriberBlocked(subscriber)) {
            throw new SubscriberIsBlockedException();
        }

        if (isDayLimitAchieved(callLimit, subscriber, ActionType.CALL)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    String.format("Лимит звонков для абонента %s исчерпан", subscriber.getMsisdn()));
        }
    }

    private void checkSubscriberCanSendSms(Subscriber subscriber) {
        if (!isOperationSupportedByTariff(subscriber.getTariff(), "smsPrice")) {
            throw new ForbiddenOperationException();
        }

        if (isSubscriberBlocked(subscriber)) {
            throw new SubscriberIsBlockedException();
        }
    }

    private boolean isSubscriberBlocked(Subscriber subscriber) {
        return StatusEnum.valueOf(subscriber.getStatus().getName()).equals(BLOCKED);
    }

    private boolean isOperationSupportedByTariff(Tariff tariff, String key) {
        return tariff.getParams().containsKey(key);
    }

    private boolean isDayLimitAchieved(Integer limit, Subscriber subscriber, ActionType actionType) {
        Integer count = subscriberActionHistoryRepository.countAllBySubscriberAndActionTypeAndProcessedAtBetween(
                subscriber, actionType, LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT),
                LocalDateTime.of(LocalDate.now(), LocalTime.MAX));

        log.info("Количество звонков " + count);

        return count >= limit;
    }
}
