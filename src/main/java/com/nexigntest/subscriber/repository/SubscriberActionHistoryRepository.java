package com.nexigntest.subscriber.repository;

import com.nexigntest.subscriber.entity.ActionType;
import com.nexigntest.subscriber.entity.Subscriber;
import com.nexigntest.subscriber.entity.SubscriberActionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface SubscriberActionHistoryRepository extends JpaRepository<SubscriberActionHistory, Long> {
    Integer countAllBySubscriberAndActionTypeAndProcessedAtBetween(
            Subscriber subscriber, ActionType actionType, LocalDateTime processedAt, LocalDateTime processedAt2);
}
