package com.nexigntest.subscriber.repository;

import com.nexigntest.subscriber.entity.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {

    Subscriber findSubscriberByMsisdn(String msisdn);
}
