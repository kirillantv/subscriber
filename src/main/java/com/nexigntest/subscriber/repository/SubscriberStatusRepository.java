package com.nexigntest.subscriber.repository;

import com.nexigntest.subscriber.entity.SubscriberStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberStatusRepository extends JpaRepository<SubscriberStatus, Long> {
    SubscriberStatus findSubscriberStatusByName(String name);
}
