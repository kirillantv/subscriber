package com.nexigntest.subscriber.controller;

import com.nexigntest.subscriber.entity.Subscriber;
import com.nexigntest.subscriber.model.ReplenishBalanceRequest;
import com.nexigntest.subscriber.model.SubscriberCallRequest;
import com.nexigntest.subscriber.model.SubscriberSmsRequest;
import com.nexigntest.subscriber.service.SubscriberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/rest/subscriber")
public class SubscriberController {

    private final SubscriberService subscriberService;

    public SubscriberController(SubscriberService subscriberService) {
        this.subscriberService = subscriberService;
    }

    @GetMapping("/info")
    public Subscriber getSubscriber(@RequestParam(name = "msisdn") String msisdn) {
        return subscriberService.getSubscriberByMsisdn(msisdn);
    }

    @GetMapping
    public List<Subscriber> getSubscribers() {
        return subscriberService.getSubscribers();
    }

    @PostMapping("/call")
    public void call(@Valid @RequestBody SubscriberCallRequest subscriberCallRequest) {
        subscriberService.makeCall(subscriberCallRequest);
    }

    @PostMapping("/sms")
    public void sendSms(@Valid @RequestBody SubscriberSmsRequest subscriberSmsRequest) {
        subscriberService.sendSms(subscriberSmsRequest);
    }

    @PostMapping("/replenish-balance")
    public void replenishBalance(@Valid @RequestBody ReplenishBalanceRequest replenishBalanceRequest) {
        subscriberService.replenishBalance(replenishBalanceRequest);
    }
}
