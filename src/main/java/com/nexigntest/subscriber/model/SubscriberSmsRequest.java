package com.nexigntest.subscriber.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SubscriberSmsRequest {
    @NotBlank
    private String msisdn;
    @NotBlank
    private String targetMsisdn;
    @NotBlank
    private String message;
}
