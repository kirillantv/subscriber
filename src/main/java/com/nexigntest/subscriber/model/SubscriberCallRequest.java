package com.nexigntest.subscriber.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SubscriberCallRequest {
    @NotBlank
    private String msisdn;
    @NotBlank
    private String targetMsisdn;
}
