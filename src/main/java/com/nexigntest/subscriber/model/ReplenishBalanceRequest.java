package com.nexigntest.subscriber.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ReplenishBalanceRequest {
    @NotBlank
    private String msisdn;
    @NotNull
    private Integer amount;
}
