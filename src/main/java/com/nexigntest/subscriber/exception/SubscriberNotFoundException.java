package com.nexigntest.subscriber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Абонент не найден")
public class SubscriberNotFoundException extends RuntimeException {
}
