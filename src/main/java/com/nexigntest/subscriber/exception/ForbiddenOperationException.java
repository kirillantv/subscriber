package com.nexigntest.subscriber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Операция не поддерживается тарифом")
public class ForbiddenOperationException extends RuntimeException {
}
