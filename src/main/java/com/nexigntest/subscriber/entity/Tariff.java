package com.nexigntest.subscriber.entity;

import lombok.Data;
import com.nexigntest.subscriber.util.HashMapConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;

@Data
@Entity
public class Tariff {
    @Id
    private Integer id;
    private String name;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> params;
}
