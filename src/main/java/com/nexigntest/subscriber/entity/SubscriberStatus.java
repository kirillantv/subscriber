package com.nexigntest.subscriber.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class SubscriberStatus {
    @Id
    private Integer id;
    private String name;

    public enum StatusEnum {
        ACTIVE,
        BLOCKED
    }
}
